package com.wenyuanhust.yamba;

import winterwell.jtwitter.Twitter;
import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

public class YambaApplication extends Application implements
	OnSharedPreferenceChangeListener {
	private static final String TAG = YambaApplication.class.getSimpleName();
	public Twitter twitter;
	private SharedPreferences prefs;
	private boolean serviceRunning;
	
	public void onCreate(){
		super.onCreate();
		this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
		this.prefs.registerOnSharedPreferenceChangeListener(this);
		Log.i(TAG, "onCreated");
	}
	
	public void onTerminate(){
		super.onTerminate();
		Log.i(TAG, "onTerminated");
	}
	
	public synchronized Twitter getTwitter(){
		if(this.twitter == null){
    		String username, password, apiRoot;
    		username = prefs.getString("username", "");
    		password = prefs.getString("password", "");
    		apiRoot = prefs.getString("apiRoot", "http://yamba.marakana.com/api");
    		
    		//Connect to twitter.com
    		if(!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)
    				&& !TextUtils.isEmpty(apiRoot)){
	    		this.twitter = new Twitter(username, password);
	    		this.twitter.setAPIRootUrl(apiRoot);
    		}
		}
		return this.twitter;
	}
	
	@Override
	public synchronized void onSharedPreferenceChanged(
			SharedPreferences sharedPreferences,
			String key) {
		// TODO Auto-generated method stub
		this.twitter = null;
	}

	public boolean isServiceRunning(){
		return serviceRunning;
	}
	
	public void setServiceRunning(boolean ServiceRunning){
		this.serviceRunning = serviceRunning;
	}
	
}