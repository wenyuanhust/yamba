package com.wenyuanhust.yamba;

import java.util.List;

//import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;

public class UpdaterService extends Service{

	private static final String TAG = "UpdaterService";
	static final int DELAY= 60000;
	private boolean runFlag = false;
	private Updater updater;
	private YambaApplication yamba;
	
	DbHelper dbHelper;
	SQLiteDatabase db;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		this.yamba = (YambaApplication)getApplication();
		this.updater = new Updater();
		
		dbHelper = new DbHelper(this);
		
		Log.d(TAG, "onCreated");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		if(!runFlag){
			this.runFlag = true;
			this.updater.start();
			((YambaApplication)super.getApplication()).setServiceRunning(true);
			
			Log.d(TAG, "onStarted");
		}
		/*
		super.onStartCommand(intent, flags, startId);
		this.yamba.setServiceRunning(true);*/
		
		return Service.START_STICKY;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		this.runFlag = false;
		this.updater.interrupt();
		this.updater = null;
		this.yamba.setServiceRunning(false);
		
		Log.d(TAG, "onDestroyed");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Thread that performs the actual update from the online service
	 * @author wenyuan
	 *
	 */
	private class Updater extends Thread{
		List<winterwell.jtwitter.Status> timeline;
		public Updater(){
			super("UpdaterService-Updater");
		}
		
		public void run(){
			UpdaterService updateService = UpdaterService.this;
			while(updateService.runFlag){
				Log.d(TAG, "Updater running");
				try{
					//some work goes here...
					//get the timeline from the cloud
					try{
						timeline = yamba.getTwitter().getFriendsTimeline();
					}catch(TwitterException e){
						Log.e(TAG, "Failed to connect to the twitter service", e);
					}
					
					//Open the database for writing
					db = dbHelper.getWritableDatabase();
					
					//Loop over the timeline and print it out
					ContentValues values = new ContentValues();
					for(winterwell.jtwitter.Status status : timeline){
						//Insert into database
						values.clear();
						//values.put(DbHelper.C_ID, (String)status.id);
						values.put(DbHelper.C_CREATED_AT, status.createdAt.getTime());
						values.put(DbHelper.C_SOURCE, status.source);
						values.put(DbHelper.C_TEXT, status.text);
						values.put(DbHelper.C_USER, status.user.name);
						db.insertOrThrow(DbHelper.TABLE, null, values);
						
						Log.d(TAG, String.format("%s: %s", status.user.name, status.text));
					}
					
					//Close the database
					db.close();
					
					Log.d(TAG, "Updater ran");
					Thread.sleep(DELAY);
				}catch(InterruptedException e){
					updateService.runFlag  = false;
				}
			}//Updater
		}
	}

}
