package com.wenyuanhust.yamba;


import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
//import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;


public class StatusActivity extends ActionBarActivity implements OnClickListener,
              TextWatcher{//, OnSharedPreferenceChangeListener{
	private static final String TAG = "StatusActivity";
	EditText editText;
	Button updateButton;
	//Twitter twitter;
	TextView textCount;
	//SharedPreferences prefs;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        
        //find views
        editText = (EditText)findViewById(R.id.editText);
        updateButton = (Button)findViewById(R.id.buttonUpdate);
        
        updateButton.setOnClickListener(this);
        
        textCount = (TextView)findViewById(R.id.textCount);
        textCount.setText(Integer.toString(140));
        textCount.setTextColor(Color.GREEN);
        editText.addTextChangedListener(this);
        
        //twitter = new Twitter("student", "password");
        //twitter.setAPIRootUrl("http://yamba.marakana.com/api");
        
        //Setup preferences
        //prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //prefs.registerOnSharedPreferenceChangeListener(this);
        
    }

    //Asynchronously post the 
    class PostToTwitter extends AsyncTask<String, Integer, String>{
    	//called to initiate the background activity
    	@Override
    	protected String doInBackground(String... statuses){
    		try{
    			YambaApplication yamba = ((YambaApplication)getApplication());
    			winterwell.jtwitter.Status status = yamba.getTwitter().updateStatus(statuses[0]);
    			//winterwell.jtwitter.Status status = ((YambaApplication)getApplication()).getTwitter().updateStatus(statuses[0]);//twitter.updateStatus(statuses[0]);
    			return status.text;
    		}catch(TwitterException e){
    			Log.e(TAG, "Failed to connect to twitter service", e);//e.toString());
    			//e.printStackTrace();
    			return "Fail to Post";
    		}
    	}
    	
    	@Override
    	protected void onProgressUpdate(Integer... values){
    		super.onProgressUpdate(values);
    	}
    	
    	@Override
    	protected void onPostExecute(String result){
    		Toast.makeText(StatusActivity.this, result, Toast.LENGTH_LONG).show();
    	}
    }
    
    /*
	private Twitter getTwitter(){
		if(twitter == null){
    		String username, password, apiRoot;
    		username = prefs.getString("username", "");
    		password = prefs.getString("password", "");
    		apiRoot = prefs.getString("apiRoot", "http://yamba.marakana.com/api");
    		
    		//Connect to twitter.com
    		twitter = new Twitter(username, password);
    		twitter.setAPIRootUrl(apiRoot);
		}
		return twitter;
	}*/
    
    //TextWatcher methods
    public void afterTextChanged(Editable statusText){
    	int count = 140 - statusText.length();
    	textCount.setText(Integer.toString(count));
    	textCount.setTextColor(Color.GREEN);
    	if(count < 10)
    		textCount.setTextColor(Color.YELLOW);
    	if(count < 0)
    		textCount.setTextColor(Color.RED);
    }
    
    public void beforeTextChanged(CharSequence s, int start, int count, int after){
    	
    }
    
    public void onTextChanged(CharSequence s, int start, int before, int after){
    	
    }
    
    
    public void onClick(View v){
    	//Update twitter status
    	try{
    		((YambaApplication)getApplication()).getTwitter().setStatus(editText.getText().toString());
    	}catch(TwitterException e){
    		Log.d(TAG, "Twitter setStatus failed: " + e);
    	}
    	/*
    	String status = editText.getText().toString();
    	new PostToTwitter().execute(status);
    	Log.d(TAG, "OnClicked");
    	*/
    }
    
    //called first time user clicks on the menu button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.status, menu);
        //return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        
        switch(id){
        	case R.id.itemServiceStart:
        		startService(new Intent(this, UpdaterService.class));
        		break;
        	case R.id.itemServiceStop:
        		stopService(new Intent(this, UpdaterService.class));
        		break;
        	case R.id.itemPrefs:
        		startActivity(new Intent(this, PrefsActivity.class));
        		break;
        	case R.id.action_settings:
        		return true;
        }

        return true;
        //return super.onOptionsItemSelected(item);
    }

    /*
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		//invalidate twitter object
		twitter = null;
	}*/
}
